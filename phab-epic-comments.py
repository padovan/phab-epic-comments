#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import subprocess
import sys
import time
import tempfile
import dateutil.parser
from datetime import datetime, timedelta
from phabricator import Phabricator

phid_author = {
    "PHID-USER-bbu4ndhblo75po4uxm6k": "Guillaume Tucker",
    "PHID-USER-vggh54ukp3j7gparyngh": "Ana Guerrero López",
    "PHID-USER-x74hj2myyv5orgnkqzjq": "Ezequiel Garcia",
    "PHID-USER-fqa2366w5ytfnrwhmgvt": "Enric Balletbò i Serra",
    "PHID-USER-jj2vhn3mtl3fcequp4kk": "Helen Koike Fornazier",
}


def extract_comments(transactions, since_epoch, until_epoch):
    comments = []
    authors = []
    for t in transactions:
        epoch = int(t['dateCreated'])
        if t['comments'] and epoch >= since_epoch and epoch <= until_epoch:
            authors.append(phid_author[t['authorPHID']])
            # Do not replace with append, or it'll break the comments order
            # and "Next week" won't appear in the bottom
            comments.insert(0, t['comments'])

    return comments, authors


def pprint_comments(task, comments, authors, print_empty, nophab, f):

    if not print_empty and not comments:
        return
    f.write("\n### " + epics[int(task)] + "\n")
    if authors:
        f.write("*By " + ", ".join(authors) + ":*\n")
    for c in comments:
        f.write("\n" + c + "\n")
    if not comments:
        f.write("No progress" + "\n")
    f.write("\n")

    if nophab:
        return
    link = "https://phabricator.collabora.com/T" + task
    f.write("[%s](%s)\n" % (link, link))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Retrieve all coments in a given week from EPIC tasks')
    parser.add_argument('-p', '--project', type=str, required=True,
                        help='the project to retrieve the comments')
    parser.add_argument('-y', '--year', type=int, default=datetime.now().year,
                        help='the year to retrieve the comments (default:current_year)')
    parser.add_argument('-a', '--all', default=False, action='store_const', const=True,
                        help="also list tasks that weren't updated")
    parser.add_argument('-o', '--output', metavar="FILE", type=str, default=None,
                        help='output to a file instead of stdout')
    parser.add_argument('-f', '--filter', metavar="STRING", type=str, default="EPIC",
                        help='filter tasks containing STRING in the title (default:%(default)s)')
    parser.add_argument('-w', '--week', type=int, default=datetime.now().isocalendar()[1],
                        help='the number of the week to retrieve the comments (default:current_week)')
    parser.add_argument('-s', '--since', type=str, default=None,
                        help='retrieve comments since a date')
    parser.add_argument('-u', '--until', type=str, default=None,
                        help='retrieve comments until a date')
    parser.add_argument('-t', '--title', metavar="STRING", type=str,
                        help='Set the title of the report')
    parser.add_argument('--nophab', default=False, action='store_const', const=True, help="don't show phabricaton information")
    args = parser.parse_args()
    phab = Phabricator()  # This will use your ~/.arcrc file
    phab.user.whoami()

    # year, first day of the week, number of the week, hour, timezone
    s_date = datetime.strptime("%s 2 %s 8" % (args.year, args.week), "%Y %w %W %H")
    e_date = datetime.strptime("%s 2 %s 8" % (args.year, args.week + 1), "%Y %w %W %H")
    e_date = e_date - timedelta(seconds=1)

    if args.since:
        s_date = dateutil.parser.parse(args.since)
    if args.until:
        e_date = dateutil.parser.parse(args.until)

    s_epoch = int(s_date.strftime('%s'))
    e_epoch = int(e_date.strftime('%s'))

    if args.output:
        fd, tmp_file_name = tempfile.mkstemp(suffix=".md")
        f = open(tmp_file_name, "w")
    else:
        f = sys.stdout

    if args.title:
        f.write("## %s\n" % args.title)
    else:
        f.write("Report: from %s to %s\n" % (str(s_date), str(e_date)))

    query = {
        "constraints": {
            "name": args.project
        }
    }

    project_phid = phab.project.search(**query).response["data"][0]["phid"]

    query = {
        "queryKey": "open",
        "constraints": {
            "projects": [project_phid]
        }
    }
    tasks = phab.maniphest.search(**query).data
    epics = {}

    for task in tasks:
        if args.filter in task["fields"]["name"]:
            epics[task["id"]] = task["fields"]["name"]

    d_transactions = phab.maniphest.gettasktransactions(ids=list(epics.keys())).response

    for task in d_transactions:
        comments, authors = extract_comments(d_transactions[task], s_epoch, e_epoch)
        pprint_comments(task, comments, authors, args.all, args.nophab, f)
    if f is not sys.stdout:
        f.close()
        os.close(fd)

    if args.output:
        try:
            subprocess.check_call(["pandoc", tmp_file_name, "-o", args.output])
            os.remove(tmp_file_name)
        except OSError:
            sys.stderr.write("WARNING: pandoc is not installed, could not convert the file format.")
            os.rename(tmp_file_name, args.output)
